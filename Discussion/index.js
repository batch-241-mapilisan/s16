alert("Hello, Batch 241!")

// ARITHMETIC OPERATORS
let x = 1397;
let y = 7831;

// Addition Operator
let sum = x + y;
console.log(`Result of addition operator: ${sum}`);

// Subtraction Operator
let difference = x - y;
console.log(`Result of the subtraction operator: ${difference}`);

// Multiplication Operator
let product = x * y;
console.log(`Result of multiplication operator: ${product}`);

// Division Operator
let quotient = x / y;
console.log(`Result of the division operator: ${quotient}`);

// Modulo Operator
let remainder = y % x;
console.log(`Result of module operator: ${remainder}`);

// ASSIGNMENT OPERATOR
// Basic Assignment Operator (=)
// The assignment operator adds the value of the right operand to a variable and assigns the result to the variable
let assignmentNumber = 8;

// Addition Assignment Operator
// assignmentNumber = assignmentNumber + 2
assignmentNumber += 2;
console.log(`Result of the addition assignment operator: ${assignmentNumber}`);

// Subtraction/Multiplication/Division (-=, *=, /=)

// Subtraction Assignment Operator
// assignmentNumber = assignmentNumber - 2
assignmentNumber -= 2;
console.log(`Result of the subtraction assignment operator: ${assignmentNumber}`);

// Multiplication Assignment Operator
// assignmentNumber = assignmentNumber * 2
assignmentNumber *= 2;
console.log(`Result of the multiplication assignment operator: ${assignmentNumber}`);

// Division Assignment Operator
// assignmentNumber = assignmentNumber / 2
assignmentNumber /= 2;
console.log(`Result of the division assignment operator: ${assignmentNumber}`);

// Multiple Operators and Parentheses
/*
    When multiple operators are applied in a single statement, it follows the PEMDAS Rule (Parenthesis, Exponents, Multiplication, Division, Addition, and Subtraction)
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(`Result of mdas operation: ${mdas}`);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(`Result of pemdas operation: ${pemdas}`);

// TYPE COERCION
/*

*/
